# Arquitecturas - Microservicios

## Microservicio de Administración de Contenido (MAC)

En la siguiente figura se muestra el diseño del microservicio actual:
![Vista de contenedores del SMAM](docs/diagrama.png)

## Prerrequisitos

- Clonar el repositorio:

  ```shell
  $ git clone https://gitlab.com/tareas-arquitectura-de-software-curso/2-microservicios.git
  $ cd microservicios
  ```

- Contar con python 3.7.5 o superior (las pruebas fueron realizadas con la
  versión 3.7.5). Se recomienda utilizar [pyenv](https://github.com/pyenv/pyenv)
  como manejador de versiones de python; una vez instalado se pueden seguir los
  siguientes comandos para instalar la versión deseada de python, esto hay que
  realizarlo en la raíz del repositorio:

  ```shell
  $ pyenv install 3.7.5
  $ pyenv local 3.7.5
  ```

- Crear un ambiente virtual para manejar las dependencias ejecutando:

  ```shell
  $ python3 -m venv venv
  ```

  o en Windows:

  ```shell
  $ py -3 -m venv venv
  ```

  Esto creará una carpeta llamada "venv" que representa nuestro ambiente virtual
  y donde instalaremos todas las dependencias.

- Activamos el ambiente virtual:

  ```shell
  $ source venv/bin/activate
  ```

  o en Windows:

  ```shell
  $ venv\Scripts\activate
  ```

- Instalamos las dependencias del sistema ejecutando:

  ```shell
  (venv)$ pip3 install -r requirements.txt
  ```

## Ejecución

Una vez instalados los prerrequisitos es momento de ejecutar el sistema
siguiendo los siguientes pasos:

1. Definimos el ambiente de Flask como desarrollo:

   ```shell
   (venv)$ export FLASK_ENV=development
   ```

1. Ejecutamos el microservicio:

   ```shell
   (venv)$ python src/run.py
   ```

## Pruebas

Para probar los endpoints se pueden realizar a través de un navegador web, una
terminal o algún software como [Insomnia](https://insomnia.rest/download/).

1. Probando con una terminal:

   ```shell
   $ curl -X GET localhost:8084/netflix/original-content
   ```

   Respuesta:

   ```shell
   $ [{"id": 1, "name": "house of cards", "type": "series", "genre": "drama", "imdb_rating": null}, {"id": 2, "name": "stranger things", "type": "series", "genre": "drama", "imdb_rating": null}]
   ```

## Versión

2.0.1 - Mayo 2020

## Autores

- **Perla Velasco**
- **Yonathan Martínez**
- **Sergio Salazar**

## Coautores

- **Einar Jhordany Serna Valdivia**
- **Edgar Bonilla Rivas**
- **Cristina Trujillo Espinoza**
- **Gerardo Ortiz Aguinaga**
